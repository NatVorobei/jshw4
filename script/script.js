// 1. Фунцкції потрібні для того, щоб можна було робити однакові дії багато разів, не повторюючи код
// 2. Аргумент передають у функцію, щоб можна було підставляти будь-які значення у змінні коли викликається функція. 
// 3. return повертає значення функції, як тільки до нього доходить функція завершується.

        let firstNumber = +prompt("First number", 0);
        let sign = prompt("Write arithmetic operation sign: + - * /");
        let secondNumber = +prompt("Second number", 0);
        let result;

        function calc(a, b, sign){
            switch(sign){
                case "+":
                    return a + b;
                case "-":
                    return a - b;
                case "*":
                    return a * b;
                case "/":
                    if (b !== 0) {
                        return a / b;
                    } else {
                        return "На нуль ділити не можна";
                    }
            }
        }

        console.log(calc(firstNumber, secondNumber, sign));
        

